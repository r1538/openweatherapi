import React from "react";
import { View, Text, Image, StyleSheet } from "react-native";

function Weather({ data }) {

  const { item } = data;
  const { weather, temp } = item;
  const [rawData] = weather

  return (
    <View style={styles.container}>

      <Image
        source={{ uri: `http://openweathermap.org/img/wn/${rawData['icon']}@2x.png` }}
        style={{ width: 100, height: 100 }}
      />

      <View>
        <Text style={styles.textM}>{rawData['description']}</Text>
        <Text style={styles.textL}>{rawData['main']}</Text>
      </View>

      <Text style={styles.textL}>{temp['day']} C</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ddd',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: "row",
    paddingHorizontal: 30,
    marginBottom: 20,
    borderRadius: 10
  },

  textL: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: "center"
  },
  textM: {
    fontSize: 15,
    fontWeight: 'bold',
    textAlign: "center"
  }
});

export default Weather;