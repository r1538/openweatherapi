import React from "react";
import { FlatList, Text, View } from "react-native";
import Weather from "./Weather";

export default function Weathers({ longitude, latitude }) {

  const [weathers, setWeathers] = React.useState([]);

  React.useEffect(() => {
    const key = "b6d552d521a6755c0b5004e7d501f556";
    const base = "https://api.openweathermap.org/data/2.5";
    const part = "hourly,current,minutely";
    const uri = `${base}/onecall?lat=${latitude}&lon=${longitude}&exclude=${part}&appid=${key}&units=${"metric"}`;

    fetch(uri)
      .then((res) => res.json())
      .then(({ daily }) => setWeathers(daily))
      .catch((err) => console.log(err));
  }, []);

  return (
    <View>
      <FlatList
        data={weathers}
        keyExtractor={(item, idx) => idx}
        renderItem={(item) => {
          return <Weather data={item} />
        }}
      />
    </View>
  )
};