import React from 'react';
import { Platform, PermissionsAndroid, StyleSheet, View, Text } from 'react-native';

import Geolocation from 'react-native-geolocation-service';
import Weathers from './components/Weathers';


const App = () => {

  const [position, setPosition] = React.useState({
    isLoading: true,
    coords: {
      longitude: 0, 
      latitude: 0
    }
  });

  async function requestPermissions() {
    if (Platform.OS === 'ios') {
      const auth = await Geolocation.requestAuthorization('whenInUse');
      if (auth === 'granted') {
        getUserLocation();
      }
    }

    if (Platform.OS === 'android') {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        getUserLocation();
      }
    }
  }

  React.useEffect(() => {
    requestPermissions();
  }, []);

  const getUserLocation = () => {
    Geolocation.getCurrentPosition(
      (position) => {
        const { longitude, latitude } = position['coords'];

        setPosition({
          isLoading: false,
          coords: { longitude, latitude }
        });
      },
      (error) => {
        // See error code charts below.
        console.log(error.code, error.message);
      },
      { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
    );
  }

  return (
    <View style={styles.container}>

      {/* <Text>
        {JSON.stringify(position, null, 2)}
      </Text> */}

      {position['isLoading'] ? (
        <Text>Loading User Location</Text>
      ) : (
        <Weathers longitude={position['coords']['longitude']} latitude={position['coords']['latitude']} />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default App;